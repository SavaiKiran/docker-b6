FROM centos:7
LABEL Author = Shubham Kalsait
ENV DB_PASSWORD=redhat
RUN yum install httpd -y; \
    mkdir /var/www/html/sample
WORKDIR /var/www/html
# SHELL ["/bin/sh", "hostname"]
USER root
COPY ./index.html ./index.html
ADD https://s3-us-west-2.amazonaws.com/studentapi-cit/index.html ./sample/index.html
RUN chmod 644 ./sample/index.html
# CMD httpd -DFOREGROUND
# CMD ["httpd", "-DFOREGROUND"]
ENTRYPOINT ["httpd", "-DFOREGROUND"]
EXPOSE 80

# ARG
# VOLUME